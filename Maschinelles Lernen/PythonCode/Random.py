from __future__ import print_function
import random
from Configuration import Config
from pandas import DataFrame
import pandas
import datetime
import numpy

conf = Config()
capital = conf.get_start_capital()



def eval_step(old_cap, val_day0, val_day0plus5):
    rand_value = random.gauss(conf.get_mid_random(), conf.get_standard_deviation_random())
    classification = classify(rand_value)
    if classification == 1: # 1 means price will be higher
        new_value = old_cap + (val_day0plus5 - val_day0)
        return new_value
    elif classification == 0:
        return old_cap # no change
    else:
        raise ValueError("no Correct classification")




def classify(value):
    classification = 0
    if value > 0.5:
        classification = 1
    return classification


def eval_all_stocks(stocks_dict):
    dict_cap = {}
    print("Start create random evaluation")
    for key,value in stocks_dict.items():
        for row in value.itertuples():
            curr_idx = row[0]
            if curr_idx == 4:
                dict_cap[row[conf.get_date_col_count_validation_set()]] = [conf.get_start_capital()] * conf.get_random_cycles_count()
            if curr_idx < 5:
                continue
            curr_date = value.get_value(curr_idx, conf.get_date_col_name())
            dict_cap[curr_date] = [0] * conf.get_random_cycles_count()

    for i in range(0, conf.get_random_cycles_count()):
        for key,value in stocks_dict.items():
            for row in value.itertuples():
                curr_idx = row[0]
                curr_date = value.get_value(curr_idx, conf.get_date_col_name())
                if curr_idx == 4:
                    dict_cap[row[conf.get_date_col_count_validation_set()]][i] = conf.get_start_capital()
                if curr_idx < 5:
                    continue
                date_one_day_ago = value.get_value(curr_idx- 1, conf.get_date_col_name())
                cap_one_day_ago = dict_cap[date_one_day_ago][i]
                price_5_days_ago = value.get_value(curr_idx - 5, conf.get_col_name_adj_col())
                curr_price = value.get_value(curr_idx, conf.get_col_name_adj_col())
                dict_cap[curr_date][i] = eval_step(cap_one_day_ago, price_5_days_ago, curr_price)

    startdate = datetime.date(conf.get_validation_year(), conf.get_validation_month(), conf.get_validation_day())
    endDate = datetime.date(conf.get_end_year(), conf.get_end_month(), conf.get_end_day())
    index = pandas.date_range(startdate, endDate)
    columns = {"EarningsMean", "EarningsMax", "EarningsMin"}
    df = DataFrame(index=index, columns=columns)
    for key,value in dict_cap.items():
        df.set_value(pandas.Timestamp(key), "EarningsMax", max(value))
        df.set_value(pandas.Timestamp(key), "EarningsMin", min(value))
        df.set_value(pandas.Timestamp(key), "EarningsMean", numpy.mean(value))

    df = df.dropna()
    # df = df.reset_index()
    print("random evaluation")
    return df



