from __future__ import print_function
from pandas import DataFrame
from copy import deepcopy
import math


class Utils(object):

    @staticmethod
    def roc_value(current, previous):
        change = (current / previous) - 1
        return change

    @staticmethod
    def roc_value_in_percent(current, previous):
        change = Utils.roc_value(current, previous)
        change_percentage = change * 100
        return change_percentage


    @staticmethod
    def remove_null_entries_in_dataframe_float(df, column_to_check_for_null_entry):
        returndf = deepcopy(df)
        for row in df.itertuples():
            try:
                currValu = 0.0 if math.isnan(float(row[column_to_check_for_null_entry])) else float(row[column_to_check_for_null_entry])
            except ValueError:
                returndf.drop(int(row[0]), inplace=True)
        return returndf

    @staticmethod
    def remove_nan_entries_in_dataframe(df, column_to_check_for_nan):
        return df.dropna(axis=0, how='all')