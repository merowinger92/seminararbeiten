# ADS.DE : 03.01.2000
# ALV.DE : 03.01.2000
# BAS.DE : 03.01.2000
# BAYN.DE : 03.01.2000
# BEI.DE : 22.7.1998
# BMW.DE : 03.01.2000
# CBK.DE : 03.01.2000
# CON.DE : 03.03.2016 missing...
# DAI.DE : 03.01.2000
# DBK.DE : 03.01.2000
# DB1.DE : 05.02.2001
# LHA.DE : 03.01.2000
# DPW.DE : 20.11.2000
# DTE.DE : 03.01.2000
# EOAN.DE : 03.01.2000
# FME.DE : 03.01.2000
# FRE.DE : 03.01.2000
# HEI.DE : 03.01.2000
# HEN.DE : 03.01.2000
# IFX.DE : 03.01.2000
# LIN.DE : 03.01.2000
# MRK.DE : 03.01.2000
# MUV2.DE : 03.01.2000 missing...
# PSM.DE : 03.01.2000
# RWE.DE : 03.01.2000
# SAP.DE : 03.01.2000
# SIE.DE : 03.01.2000
# TKA.DE : 03.01.2000
# VOW.DE : 24.03.1995
# VNA.DE : 04.09.2015
from __future__ import print_function
from pandas_datareader import data as dataDL
import pandas
import datetime
from datetime import datetime
import os
import math
from Configuration import Config
from Utils import Utils
import fix_yahoo_finance
import numpy
import glob
import numpy as np
import random

symbols = ["ADS.DE", "ALV.DE", "BAS.DE", "BAYN.DE", "BEI.DE", "BMW.DE", "CBK.DE", "DAI.DE", "DBK.DE", "DB1.DE", "LHA.DE",
           "DPW.DE", "DTE.DE", "EOAN.DE", "FME.DE", "FRE.DE", "HEI.DE", "HEN.DE", "IFX.DE", "LIN.DE", "MRK.DE", "MUV2.DE", "PSM.DE", "RWE.DE",
           "SAP.DE", "SIE.DE", "TKA.DE", "VOW.DE", "VNA.DE"]

class Data_provider(object):
    # static
    @staticmethod
    def download_data(path, start , end):
        conf = Config()
        if not os.path.exists(path):
            os.makedirs(path)
        for x in symbols:
            try:
                dataframe = dataDL.get_data_yahoo(x, start, end)
                dataframe.to_csv(path + conf.get_path_seperator() + x + '.csv')
            except Exception as inst:
                print(inst)
        print("Data downloaded")

    # instance variables
    path = ""
    origin_stocks = {}
    transformed_stocks = {}
    daily_returns_all = {}
    daily_returns_median = {}
    input_stocks_dict = {}
    input_train_array_data_FF = []
    input_train_array_class_FF = []
    input_test_array_data_FF = []
    input_test_array_class_FF = []

    def __init__(self, path, startDate, endDate):
        conf = Config()
        self.path = path
        self.origin_stocks = self.create_stocks_dictonary_inc_dataFrames(self.path, startDate, endDate)
        self.sum_of_all_stocks_df = self.create_cumulative_dataframe_over_all_data(self.origin_stocks, startDate, endDate)
        self.daily_returns_all = self.build_daily_roc_dictionary(self.origin_stocks, startDate, endDate)
        self.daily_returns_median = self.determine_daily_median(self.daily_returns_all)
        self.input_stocks_dict = self.create_input_dataframe(self.origin_stocks)
        self.input_train_array_data_FF, self.input_train_array_class_FF, \
        self.input_test_array_data_FF, self.input_test_array_class_FF = self.create_train_test_data_array_for_FF_net(self.daily_returns_all)
        self.batch_count_FF = 0
        self.stocks_dataframe_dict_validation = self.get_stocks_dict_dataframe_validation_data(self.origin_stocks)

    def get_stocks_dict_dataframe_validation_data(self,all_stocks_dict):
        conf = Config()
        stock_dataframe_dict_validation_data = {}
        path = conf.get_folder_path() + conf.get_path_seperator() + "Validation" + conf.get_path_seperator()
        if os.path.exists(path) and len(glob.glob(path+ "*.csv")) > 0:
            print("Validation Data already there...")
            dirContent = os.listdir(path)
            for entry in dirContent:
                if os.path.isfile(path + conf.get_path_seperator() + entry) and entry.endswith(".csv"):
                    print("Reading validation Dataset of share: ", entry.replace(".csv",""))
                    df = pandas.read_csv(path + conf.get_path_seperator() + entry,index_col=0)
                    stock_dataframe_dict_validation_data[entry.replace(".csv","")] = df
            return stock_dataframe_dict_validation_data
        print("No Validation data there....")
        print("Creating Validation Dataset...")
        for key, value in all_stocks_dict.items():
            index = 0
            cols = {conf.get_date_col_name(), conf.get_col_name_adj_col()}
            dataframe_val_per_stock = pandas.DataFrame(columns=cols)
            count = 0
            for row in value.itertuples():
                date_as_string = str(row[conf.get_day_column_number()]).split("-")
                date = datetime(int(date_as_string[0]), int(date_as_string[1]), int(date_as_string[2]))
                val_date = datetime(conf.get_validation_year(), conf.get_validation_month(), conf.get_validation_day())
                if val_date > date:
                    continue
                dataframe_val_per_stock.loc[count] = pandas.Series({conf.get_date_col_name() :row[conf.get_day_column_number()], conf.get_col_name_adj_col(): row[conf.get_adj_close_col()]})
                count += 1
            dataframe_val_per_stock = Utils.remove_nan_entries_in_dataframe(dataframe_val_per_stock, 1522)
            stock_dataframe_dict_validation_data[key] = dataframe_val_per_stock
        print("Saving validation Dataset...")
        if not os.path.exists(path):
            os.makedirs(path)
        for key, value in stock_dataframe_dict_validation_data.items():
            value.to_csv(path + key + ".csv")
        return stock_dataframe_dict_validation_data

    def create_train_test_data_array_for_FF_net(self, all_data_dict):
        conf = Config()
        train_x = []
        train_y = []
        test_x = []
        test_y = []
        all_features = []
        end_date = datetime(conf.get_validation_year(), conf.get_validation_day(), conf.get_validation_month())
        for key, value in self.get_input_dataframe_dict().items():
            for row in value.itertuples():
                curr_date_as_string = str(row[conf.get_day_column_number()]).split("-")
                curr_date = datetime(int(curr_date_as_string[0]), int(curr_date_as_string[1]), int(curr_date_as_string[2]))
                if curr_date >= end_date:
                    break
                one_set = [0.0] * 10
                for i in range(0, 10):
                    one_set[i] = row[i + 2]
                all_features.append(one_set)

        random.shuffle(all_features)
        all_fea = np.array(all_features)

        train_size = int(len(all_features) * 0.8)
        print("Count of all sets: ", len(all_features))
        for i in range(0, train_size):
            set_x = all_features[i][:8]
            set_y = all_features[i][8:len(all_features[i])]
            train_x.append(set_x)
            train_y.append(set_y)
        for j in range(train_size, len(all_features)-1):
            set_x = all_features[j][:8]
            set_y = all_features[j][8:len(all_features[j])]
            test_x.append(set_x)
            test_y.append(set_y)

        train_x = np.array(train_x)
        train_y = np.array(train_y)
        test_x = np.array(test_x)
        test_y = np.array(test_y)

        print("Count Trainigsets: ", len(train_x))
        print("Count Testsets: ", len(test_x))
        return train_x, train_y, test_x, test_y

    def get_training_data_FF(self):
        return self.input_train_array_data_FF, self.input_train_array_class_FF

    def get_training_data_count(self):
        return len(self.input_train_array_data_FF)

    def get_train_data_batch_FF(self):
        conf = Config()
        start = self.batch_count_FF
        end = 0
        if (self.batch_count_FF + conf.get_batch_size_FF()) > len(self.input_train_array_data_FF):
            end = len(self.input_train_array_data_FF)
        else:
            end = self.batch_count_FF + conf.get_batch_size_FF()

        train_x = self.input_train_array_data_FF[start:end]
        train_y = self.input_train_array_class_FF[start:end]
        self.batch_count_FF += conf.get_batch_size_FF()
        return train_x, train_y

    def get_test_data_FF(self):
        return self.input_test_array_data_FF, self.input_test_array_class_FF

    def get_current_batch_count(self):
        return self.batch_count_FF

    def get_batch_num_FF(self):
        conf = Config()
        count_features = len(self.input_train_array_data_FF)
        return int(count_features//conf.get_batch_size_FF())

    def reset_batch_counter_FF(self):
        print("Reset Batchcount")
        self.batch_count_FF = 0

    def create_stocks_dictonary_inc_dataFrames(self, path, start, end):
        conf = Config()
        if not os.path.exists(path):
            os.makedirs(path)
        dirContent = glob.glob(path + conf.get_path_seperator()+"*.csv")
        stocks = {}
        if not len(dirContent) > 0:
            print("No Data, Downloading Data...")
            Data_provider.download_data(path, start, end)
        dirContent = os.listdir(path)
        for entry in dirContent:
            if os.path.isfile(path + conf.get_path_seperator() +entry) and entry.endswith(".csv"):
                df = pandas.read_csv(path + conf.get_path_seperator() + entry)
                # remove null value for public holidays...
                df = Utils.remove_null_entries_in_dataframe_float(df, conf.get_adj_close_col())
                stocks[entry.replace(".csv","")] = df
        print("Ready reading in raw data....")
        return stocks

    def create_cumulative_dataframe_over_all_data(self, stocks, startDate, endDate):
        conf = Config()
        columnName = {conf.get_col_name_sum_all_stocks()}
        index = pandas.date_range(startDate,endDate)
        df = pandas.DataFrame(index=index, columns=columnName)
        for key, value in stocks.items():
            for row in value.itertuples():
                erg = 0.0 if math.isnan(float(df.get_value(pandas.Timestamp(row[1]),"sumOverAllStocks"))) else float(df.get_value(pandas.Timestamp(row[1]),"sumOverAllStocks"))
                currStockClose = 0.0 if math.isnan(float(row[conf.get_adj_close_col()])) else float(row[conf.get_adj_close_col()])
                erg += currStockClose
                df.set_value(pandas.Timestamp(row[1]),conf.get_col_name_sum_all_stocks(), erg)
        df = df.dropna()
        df = df.reset_index()
        print("made cumulative dataframe over all data")
        return df

    def build_daily_roc_dictionary(self, stocks, startDate, endDate):
        # loop over
        conf = Config()
        dict = {}
        daterange = pandas.date_range(startDate,endDate)
        for i in daterange:
            dict[i.strftime('%Y-%m-%d')] = []
        for key, value in stocks.items():
            for row in value.itertuples():
                if row[0] == 0:
                    continue
                prev_index = row[0] - 1
                prev_value = float(value.get_value(prev_index, conf.get_col_name_adj_col()))
                curr_value = float(row[conf.get_adj_close_col()])
                roc = Utils.roc_value_in_percent(curr_value, prev_value)
                dict[row[conf.get_day_column_number()]].append(roc)

        for key, value in dict.items():
            dict[key].sort()
        print("ready building ROCs for all stocks...")
        return dict

    def determine_daily_median(self, all_daily_roc):
        dict = {}
        for key, value in all_daily_roc.items():
            dict[key] = numpy.median(value)
        return dict

    def classification(self, currVal, five_days_ago):
        if currVal > five_days_ago:
            return int(0), int(1)
        else:
            return int(1), int(0)

    def create_input_dataframe(self,stocks):
        conf = Config()
        path = conf.get_folder_path() + conf.get_path_seperator() + "Classified" + conf.get_path_seperator()
        dict = {}
        reload = False
        if os.path.exists(path) and len(glob.glob(path+ "*.csv")) > 0:
            dirContent = os.listdir(path)
            for entry in dirContent:
                if os.path.isfile(path + conf.get_path_seperator() + entry) and entry.endswith(".csv"):
                    reload = True
                    print("Reading Input Dateframe of share: ", entry.replace(".csv",""))
                    df = pandas.read_csv(path + conf.get_path_seperator() + entry,index_col=0)
                    dict[entry.replace(".csv","")] = df
            return dict
        print("creating Inputdata per share...")
        col_names = ["Date","roc1", "roc2", "roc3", "roc4", "roc5", "roc10", "roc15", "roc20", "high", "down"]
        for key, value  in stocks.items():
            print("Current share: ", key)
            df = pandas.DataFrame(columns=col_names)
            for i in range(20, len(value.index) - 5):
                val_curr = value.get_value(i, conf.get_col_name_adj_col())
                val_one_day_ago = value.get_value(i - 1, conf.get_col_name_adj_col())
                val_two_day_ago = value.get_value(i - 2, conf.get_col_name_adj_col())
                val_three_day_ago = value.get_value(i - 3, conf.get_col_name_adj_col())
                val_four_day_ago = value.get_value(i - 4, conf.get_col_name_adj_col())
                val_five_day_ago = value.get_value(i - 5, conf.get_col_name_adj_col())
                val_ten_day_ago = value.get_value(i - 10, conf.get_col_name_adj_col())
                val_fiveteen_day_ago = value.get_value(i - 15, conf.get_col_name_adj_col())
                val_twenty_day_ago = value.get_value(i - 20, conf.get_col_name_adj_col())
                date = value.get_value(i, conf.get_day_column_name())
                roc1 = Utils.roc_value_in_percent(val_curr, val_one_day_ago)
                roc2 = Utils.roc_value_in_percent(val_curr, val_two_day_ago)
                roc3 = Utils.roc_value_in_percent(val_curr, val_three_day_ago)
                roc4 = Utils.roc_value_in_percent(val_curr, val_four_day_ago)
                roc5 = Utils.roc_value_in_percent(val_curr, val_five_day_ago)
                roc10 = Utils.roc_value_in_percent(val_curr, val_ten_day_ago)
                roc15 = Utils.roc_value_in_percent(val_curr, val_fiveteen_day_ago)
                roc20 = Utils.roc_value_in_percent(val_curr, val_twenty_day_ago)
                val_five_days_ahead = value.get_value(i + 5, conf.get_col_name_adj_col())
                high, down = self.classification(val_curr, val_five_days_ahead)
                df.loc[i - 19] = pandas.Series({"Date": date, "roc1": roc1, "roc2": roc2, "roc3": roc3,
                               "roc4": roc4, "roc5": roc5, "roc10": roc10, "roc15": roc15,
                               "roc20": roc20, "high": high, "down": down })
            df.reset_index()
            dict[key] = df
        print("Ready calculating input data")
        print("saving classified Data")
        if not os.path.exists(path):
            os.makedirs(path)
        for key, value in dict.items():
            value.to_csv(path + key + ".csv")
        return dict

    #getter
    def get_stocks_dataframe_dict_validation(self):
        return self.stocks_dataframe_dict_validation

    def get_dictonary_all_stocks(self):
        return self.origin_stocks

    def get_cumulative_stocks_dataframe(self):
        return self.sum_of_all_stocks_df

    def get_daily_returns_all(self):
        return self.daily_returns_all

    def get_daily_returns_median(self):
        return self.daily_returns_median

    def get_input_dataframe_dict(self):
        return self.input_stocks_dict