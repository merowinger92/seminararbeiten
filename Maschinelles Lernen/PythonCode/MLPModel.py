from __future__ import print_function
import tensorflow as tf
from Configuration import Config
from data_provider import Data_provider
import os
import datetime
from Utils import Utils
from pandas import DataFrame
import pandas

conf = Config()
x_train_data = tf.placeholder("float",[None, conf.get_input_cols_model()])
y_train_label_classes = tf.placeholder("float",[None, conf.get_output_classes()])
input_layer_size = conf.get_input_cols_model()
neuron_hidden_layer_1 = 1000
output_classes = conf.get_output_classes()
batch_size = conf.get_batch_size_FF()
hm_epoches = conf.get_FF_Epoches()

def MLP_neural_network_model(data):
    hidden_layer1 = {'weights': tf.Variable(tf.random_normal([input_layer_size, neuron_hidden_layer_1]), name="hl1_weights"),
                     'biases': tf.Variable(tf.zeros([neuron_hidden_layer_1]), name="hl1_bias")}

    neurons_hl2 = 1000
    hidden_layer2 = {'weights': tf.Variable(tf.random_normal([neuron_hidden_layer_1, neurons_hl2]), name="hl2_weights"),
                     'biases': tf.Variable(tf.zeros([neurons_hl2]), name="hl2_bias")}

    # neurons_hl3 = 500
    # hidden_layer3 = {'weights': tf.Variable(tf.random_normal([neurons_hl2, neurons_hl3]), name="hl3_weights"),
    #                  'biases': tf.Variable(tf.zeros([neurons_hl3]), name="hl3_bias")}

    output_layer = {'weights': tf.Variable(tf.random_normal([neurons_hl2, output_classes]), name="output_weights"),
                    'biases': tf.Variable(tf.zeros([output_classes]), name="output_bias")}

    hl1 = tf.add(tf.matmul(data, hidden_layer1['weights']), hidden_layer1['biases'])
    hl1 = tf.nn.relu(hl1)

    hl2 = tf.add(tf.matmul(hl1, hidden_layer2['weights']), hidden_layer2['biases'])
    hl2 = tf.nn.relu(hl2)

    # hl3 = tf.add(tf.matmul(hl2, hidden_layer3['weights']), hidden_layer3['biases'])
    # hl3 = tf.nn.relu(hl3)

    output =  tf.matmul(hl2, output_layer['weights'])+ output_layer['biases']
    # output = tf.nn.softmax(output)

    return output


def train_and_eval_MLP_neural_network(data):
    lowest_loss_train = conf.get_start_loss()
    lowest_loss_test = conf.get_start_loss()
    # model
    prediction = MLP_neural_network_model(x_train_data)
    prediction_op = tf.nn.softmax(prediction, name="prediction_op")
    # end model

    costs = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=prediction, labels=y_train_label_classes))
    optimizer = tf.train.AdamOptimizer().minimize(costs)

    with tf.Session() as session:
        session.run(tf.global_variables_initializer())

        for epoche in range(0,hm_epoches):
            epoch_loss = 0
            batchcount = data.get_batch_num_FF()
            for i in range(0, batchcount):
                x_trained_data, y_labeled_data = data.get_train_data_batch_FF()
                _, cost = session.run([optimizer, costs], feed_dict= {x_train_data: x_trained_data, y_train_label_classes: y_labeled_data})
                epoch_loss += cost
            print("Epoch ", epoche + 1, " completed of ", hm_epoches, ", current loss: ", epoch_loss)
            data.reset_batch_counter_FF()

            corr_pred = tf.equal(tf.argmax(prediction_op, 1), tf.argmax(y_train_label_classes, 1))
            accuracy_test = tf.reduce_mean(tf.cast(corr_pred, 'float'))
            accuracy_training = tf.reduce_mean(tf.cast(corr_pred, 'float'))
            test_x, test_y = data.get_test_data_FF()
            train_x, train_y = data.get_training_data_FF()

            loss_test = session.run(costs, feed_dict={x_train_data: test_x, y_train_label_classes: test_y})
            print("Test Loss: ", loss_test)
            if epoch_loss < lowest_loss_train and loss_test < lowest_loss_test:
                lowest_loss_train = epoch_loss
                lowest_loss_test = loss_test
                save_Model(session, epoche)

            print("Accuracy Traing: ", accuracy_training.eval({x_train_data: train_x, y_train_label_classes: train_y}), end=" ")
            print("Accuracy Test: ", accuracy_test.eval({x_train_data: test_x, y_train_label_classes: test_y}))
        print("Finished Training...")
        df = eval_all_stocks(data.get_stocks_dataframe_dict_validation(), data.get_dictonary_all_stocks(), session)
        return df



def save_Model(session, epoch):
    print("Saving current state...")
    saver = tf.train.Saver()
    path = conf.get_folder_path()+ conf.get_path_seperator() + "Model"
    if not os.path.exists(path):
        os.makedirs(path)
    bii = saver.save(session, path + conf.get_path_seperator()+"FFN-model")
    print(bii)


def load_model(sess, path):
    print("Reload model...")
    model_folder = path + "Model"
    path = model_folder  + conf.get_path_seperator() + "FFN-model"
    new_saver = tf.train.import_meta_graph(path + ".meta")
    new_saver.restore(sess, tf.train.latest_checkpoint(model_folder + conf.get_path_seperator()))
    graph = tf.get_default_graph()
    prediction_op = graph.get_tensor_by_name("prediction_op:0")
    return prediction_op


def predict(sess, prediction_op, path, roc1, roc2, roc3, roc4, roc5, roc10, roc15, roc20):
    val_data = [0]*8
    val_data[0] = roc1
    val_data[1] = roc2
    val_data[2] = roc3
    val_data[3] = roc4
    val_data[4] = roc5
    val_data[5] = roc10
    val_data[6] = roc15
    val_data[7] = roc20
    feed_x = [val_data]
    feed_dict = {x_train_data: feed_x}
    return_value = sess.run(prediction_op, feed_dict=feed_dict)
    return return_value


def single_prediction(session, prediction_op, roc1, roc2, roc3, roc4, roc5, roc10, roc15, roc20):
    print("single Prediction with values: ", roc1, roc2, roc3, roc4, roc5, roc10, roc15, roc20)
    path = conf.get_folder_path() + conf.get_path_seperator()
    prediction = predict(session, prediction_op, path, roc1, roc2, roc3, roc4, roc5, roc10, roc15, roc20)

    return prediction[0][0]


def eval_step(session, prediction_op, old_cap, val_day0, val_day0plus5,roc1, roc2, roc3, roc4, roc5, roc10, roc15, roc20):
    prediction = single_prediction(session, prediction_op, roc1, roc2, roc3, roc4, roc5, roc10, roc15, roc20)
    if prediction > 0.5:
        new_value = old_cap + (val_day0plus5 - val_day0)
        return new_value
    elif prediction <=0.5:
        return old_cap
    else:
        raise ValueError("no Correct classification")


def eval_all_stocks(stocks_dict_validation, stocks_dict, session):
    path = conf.get_folder_path() + conf.get_path_seperator()
    prediction_op = load_model(session, path)
    dict_cap = {}
    dict_with_20_days_defore_per_stock = {}
    startdate = datetime.datetime(conf.get_validation_year(), conf.get_validation_month(), conf.get_validation_day())
    for key,value in stocks_dict.items():
        print("building 20 days back for: ", key)
        array_20_days = [0]* 20
        index = 0
        for row in value.itertuples():
            date = str(row[conf.get_day_column_number()]).split("-")
            curr_date = datetime.datetime(int(date[0]), int(date[1]), int(date[2]))
            index = row[0]
            if startdate <= curr_date:
                break
        for i in range(0,20):
            array_20_days[i] = value.get_value(index - i - 1, conf.get_col_name_adj_col())
        dict_with_20_days_defore_per_stock[key] = array_20_days
    for key, value in stocks_dict_validation.items():
        print("Starting simulation for stock: ", key)
        for row in value.itertuples():
                array_with_20_days_ago = dict_with_20_days_defore_per_stock[key]
                val_1_day_ago = 0
                val_2_day_ago = 0
                val_2_day_ago = 0
                val_3_day_ago = 0
                val_4_day_ago = 0
                val_5_day_ago = 0
                val_10_day_ago = 0
                val_15_day_ago = 0
                val_20_day_ago = 0
                index_1_day_ago = row[0] - 1
                index_2_day_ago = row[0] - 2
                index_3_day_ago = row[0] - 3
                index_4_day_ago = row[0] - 4
                index_5_day_ago = row[0] - 5
                index_10_day_ago = row[0] - 10
                index_15_day_ago =  row[0] - 15
                index_20_day_ago = row[0] - 20
                # one day ago...
                if index_1_day_ago < 0:
                    index_1_day_ago = int(index_1_day_ago * (-1))
                    val_1_day_ago = array_with_20_days_ago[index_1_day_ago - 1]
                else:
                    val_1_day_ago = value.get_value(index_1_day_ago, conf.get_col_name_adj_col())
                # two days ago...
                if index_2_day_ago < 0:
                    index_2_day_ago = int(index_2_day_ago * (-1))
                    val_2_day_ago = array_with_20_days_ago[index_2_day_ago - 1]
                else:
                    val_2_day_ago = value.get_value(index_2_day_ago, conf.get_col_name_adj_col())
                # 3 days ago
                if index_3_day_ago < 0:
                    index_3_day_ago = int(index_3_day_ago * (-1))
                    val_3_day_ago = array_with_20_days_ago[index_3_day_ago - 1]
                else:
                    val_3_day_ago = value.get_value(index_3_day_ago, conf.get_col_name_adj_col())
                # 4 days ago
                if index_4_day_ago < 0:
                    index_4_day_ago = int(index_4_day_ago * (-1))
                    val_4_day_ago = array_with_20_days_ago[index_4_day_ago - 1]
                else:
                    val_4_day_ago = value.get_value(index_4_day_ago, conf.get_col_name_adj_col())
                # 5 days ago
                if index_5_day_ago < 0:
                    index_5_day_ago = int(index_5_day_ago * (-1))
                    val_5_day_ago = array_with_20_days_ago[index_5_day_ago - 1]
                else:
                    val_5_day_ago = value.get_value(index_5_day_ago, conf.get_col_name_adj_col())
                # 10 days ago
                if index_10_day_ago < 0:
                    index_10_day_ago = int(index_10_day_ago * (-1))
                    val_10_day_ago = array_with_20_days_ago[index_10_day_ago - 1]
                else:
                    val_10_day_ago = value.get_value(index_10_day_ago, conf.get_col_name_adj_col())
                # 15 days ago
                if index_15_day_ago < 0:
                    index_15_day_ago = int(index_15_day_ago * (-1))
                    val_15_day_ago = array_with_20_days_ago[index_15_day_ago - 1]
                else:
                    val_15_day_ago = value.get_value(index_15_day_ago, conf.get_col_name_adj_col())
                # 20 days ago
                if index_20_day_ago < 0:
                    index_20_day_ago = int(index_20_day_ago * (-1))
                    val_20_day_ago = array_with_20_days_ago[index_20_day_ago - 1]
                else:
                    val_20_day_ago = value.get_value(index_20_day_ago, conf.get_col_name_adj_col())

                max_index = len(value.index)
                wanted_idx = row[0] + 5
                if  wanted_idx >= max_index:
                    break
                new_date = value.get_value(row[0] + 5, conf.get_date_col_name())
                old_cap = 0
                if row[0] == 0:
                    old_cap = conf.get_start_capital()
                    dict_cap["2016-01-08"] = old_cap
                else:
                    date_before = value.get_value(row[0] + 4, conf.get_date_col_name())
                    old_cap = dict_cap[date_before]
                curr_val = value.get_value(row[0], conf.get_col_name_adj_col())
                roc_1_day_ago = Utils.roc_value_in_percent(curr_val, val_1_day_ago)
                roc_2_day_ago = Utils.roc_value_in_percent(curr_val, val_2_day_ago)
                roc_3_day_ago = Utils.roc_value_in_percent(curr_val, val_3_day_ago)
                roc_4_day_ago = Utils.roc_value_in_percent(curr_val, val_4_day_ago)
                roc_5_day_ago = Utils.roc_value_in_percent(curr_val, val_5_day_ago)
                roc_10_day_ago = Utils.roc_value_in_percent(curr_val, val_10_day_ago)
                roc_15_day_ago = Utils.roc_value_in_percent(curr_val, val_15_day_ago)
                roc_20_day_ago = Utils.roc_value_in_percent(curr_val, val_20_day_ago)

                val_0_plus_5_days = value.get_value(row[0] + 5, conf.get_col_name_adj_col())
                new_val = eval_step(session, prediction_op, old_cap, curr_val, val_0_plus_5_days,roc_1_day_ago, roc_2_day_ago, roc_3_day_ago, roc_4_day_ago, roc_5_day_ago, roc_10_day_ago, roc_15_day_ago, roc_20_day_ago)
                dict_cap[new_date] = new_val

    endDate = datetime.date(conf.get_end_year(), conf.get_end_month(), conf.get_end_day())
    index = pandas.date_range(startdate, endDate)
    columns = {"Earnings"}
    df = DataFrame(index=index, columns=columns)
    for key,value in dict_cap.items():
        df.set_value(pandas.Timestamp(key), "Earnings", value)

    df = df.dropna()
    print("FeedForward Evaluation done...")
    return df