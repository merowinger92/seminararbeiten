from __future__ import print_function
from data_provider import Data_provider
from Configuration import Config
import datetime
import matplotlib.pyplot as plt
from matplotlib import style
from pandas import DataFrame
import timeit
import time
import os
import MLPModel
import Random
import tensorflow as tf
style.use("ggplot")
import pandas


# load config
config = Config()

# # init startDate and EndDate as datetim format
# startDate = datetime.date(config.get_start_year(), config.get_start_month(), config.get_start_day())
# endDate = datetime.date(config.get_end_year(), config.get_end_month(), config.get_end_day())
#
#
# data = Data_provider(config.get_folder_path(), startDate, endDate)
# df = MLPModel.train_and_eval_MLP_neural_network(data)
# path = config.get_folder_path() + config.get_path_seperator() + "MLPModel.csv"
# df.to_csv(path)
#
#
# df = Random.eval_all_stocks(data.get_stocks_dataframe_dict_validation())
#
# path = config.get_folder_path() + config.get_path_seperator() + "RandomResults.csv"
# df.to_csv(path)
path = config.get_folder_path()
df1 = pandas.read_csv(path + config.get_path_seperator() + "Results" + config.get_path_seperator() + "3Layer300Neuronen.csv",index_col=0)

df2 = pandas.read_csv(path + config.get_path_seperator() + "Results" + config.get_path_seperator() + "3Layer1000Neuronen.csv",index_col=0)

df3 = pandas.read_csv(path + config.get_path_seperator() + "Results" + config.get_path_seperator() + "3Layer500Neuronen.csv",index_col=0)


df1['500 Neuronen'] = df3['500 Neuronen']
df1['1000 Neuronen'] = df2['1000 Neuronen']


df1.to_csv(path + config.get_path_seperator() + "Results" + config.get_path_seperator()+ "mauzi.csv")

df1.plot()

plt.title("3 versteckte Schichten")
plt.xlabel("Zeit")
plt.ylabel("Geldeinheiten")
plt.show()