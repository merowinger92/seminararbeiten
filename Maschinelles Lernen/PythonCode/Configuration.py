from __future__ import print_function
import datetime

# '''
# Steuer Parameter:
# @FolderPath: Pfad wo die Daten abgelegt werden sollen
# @StartMonth: Start Monat der Daten (als Integer)
# @StartDay: Start Tag der Daten (als Integer)
# @StartYear: Start JAhr der Daten (als Integer)
# @EndMonth: End Monat der Daten (als Integer)
# @EndDay: End Monat der Daten (als Integer)
# @EndYear: End Jahr der Daten (als Integer)
# '''

FOLDER_PATH = "C:\\Users\\mathi\\OneDrive\\Dokumente\\Uni Mainz\\Semester 3\\seminararbeiten\\Maschinelles Lernen\\PythonCode\\Data"
PATH_SEPARATOR = "\\" # \\ usually on Windows, / on UNIX
START_MONTH = 1
START_DAY = 3
START_YEAR = 2000
END_MONTH = datetime.date.today().month
END_DAY = datetime.date.today().day
END_YEAR = datetime.date.today().year
VALIDATION_DATA_MONTH = 1
VALIDATION_DATA_YEAR = 2016
VALIDATION_DATA_DAY = 1
TRAIN_PERCENTAGE = 0.9
COLUMN_DATE = 1
COLUMN_ADJ_CLOSE = 6
COLUMN_NAME_ADJ_CLOSE = "Adj Close"
COLUMN_NAME_SUM_ALL_STOCKS = "sumOverAllStocks"
INPUT_DATA_DAYS_BACK = 10
COLUMN_NAME_DATA_INPUT = "Date"
INPUT_COLS = 8
OUTPUT_CLASSES = 2

DATE_COLUMN_VALIDATION_SET = 1
ADJ_CLOSE_COLUMN_VALIDATION_SET = 2

BATCH_SIZE_FF = 20
EPOCHES_FF = 1
LOSS_START_FF = 1000000000

RNN_SEQUENCE_IN_DAYS = 20
EPOCHES_RNN = 100
BATCH_SIZE_RNN = 12
RNN_SIZE = 20

MID = 0.5
STANDARD_DEVIATION = 0.1
RANDOM_CYCLES = 10000

START_CAPITAL = 1000



class Config(object):
    # init everything..
    def __init__(self):
        self.folder_path = FOLDER_PATH
        self.start_month = START_MONTH
        self.start_day = START_DAY
        self.start_year = START_YEAR
        self.end_month = END_MONTH
        self.end_day = END_DAY
        self.end_year = END_YEAR
        self.adj_close_col = COLUMN_ADJ_CLOSE
        self.sum_all_stocks = COLUMN_NAME_SUM_ALL_STOCKS
        self.day_back_input = INPUT_DATA_DAYS_BACK
        self.date_col = COLUMN_DATE
        self.date_col_name = COLUMN_NAME_DATA_INPUT
        self.col_name_adj_close = COLUMN_NAME_ADJ_CLOSE
        self.input_cols_model = INPUT_COLS
        self.output_classes = OUTPUT_CLASSES
        self.batch_size_ff = BATCH_SIZE_FF
        self.ff_epoches = EPOCHES_FF
        self.val_day = VALIDATION_DATA_DAY
        self.val_month = VALIDATION_DATA_MONTH
        self.val_year = VALIDATION_DATA_YEAR
        self.rnn_sequence = RNN_SEQUENCE_IN_DAYS
        self.rnn_epoches = EPOCHES_RNN
        self.rnn_batch_size = BATCH_SIZE_RNN
        self.rnn_size = RNN_SIZE
        self.mid = MID
        self.standard_deviation = STANDARD_DEVIATION
        self.start_capital = START_CAPITAL
        self.path_seperator = PATH_SEPARATOR
        self.date_col_count_validation = DATE_COLUMN_VALIDATION_SET
        self.adj_close_col_validation = ADJ_CLOSE_COLUMN_VALIDATION_SET
        self.random_cycles = RANDOM_CYCLES
        self.start_loss_ff = LOSS_START_FF

    # getter
    def get_start_loss(self):
        return self.start_loss_ff

    def get_random_cycles_count(self):
        return self.random_cycles

    def get_date_col_count_validation_set(self):
        return self.date_col_count_validation

    def get_adj_close_col_count_validation(self):
        return self.adj_close_col_validation

    def get_path_seperator(self):
        return self.path_seperator

    def get_date_col_name(self):
        return self.date_col_name

    def get_start_capital(self):
        return self.start_capital

    def get_standard_deviation_random(self):
        return self.standard_deviation

    def get_mid_random(self):
        return self.mid

    def get_validation_day(self):
        return self.val_day

    def get_validation_month(self):
        return self.val_month

    def get_validation_year(self):
        return self.val_year

    def get_folder_path(self):
        return self.folder_path

    def get_start_month(self):
        return self.start_month

    def get_start_day(self):
        return self.start_day

    def get_start_year(self):
        return self.start_year

    def get_end_month(self):
        return self.end_month

    def get_end_day(self):
        return self.end_day

    def get_end_year(self):
        return self.end_year

    def get_adj_close_col(self):
        return self.adj_close_col

    def get_col_name_sum_all_stocks(self):
        return self.sum_all_stocks

    def get_day_count_input_data(self):
        return self.day_back_input

    def get_day_column_number(self, reload=False):
        if reload == True:
            return self.date_col + 1
        return self.date_col

    def get_day_column_name(self):
        return self.date_col_name

    def get_col_name_adj_col(self):
        return self.col_name_adj_close

    def get_input_cols_model(self):
        return self.input_cols_model

    def get_output_classes(self):
        return self.output_classes

    def get_batch_size_FF(self):
        return self.batch_size_ff

    def get_FF_Epoches(self):
        return self.ff_epoches

    def get_RNN_sequence_in_days(self):
        return self.rnn_sequence

    def get_epoches_rnn(self):
        return self.rnn_epoches

    def get_batch_size_rnn(self):
        return self.rnn_batch_size

    def get_size_rnn(self):
        return self.rnn_size