import tensorflow as tf
from tensorflow.python.ops import rnn, rnn_cell_impl
from Configuration import Config

conf = Config()
chunk_size = conf.get_input_cols_model()
n_chunks = conf.get_RNN_sequence_in_days()
hm_epoches = conf.get_epoches_rnn()
n_classes = conf.get_output_classes()
batch_size = conf.get_batch_size_rnn()
rnn_size = conf.get_size_rnn()

x_input = tf.placeholder('float', [None, n_chunks,chunk_size])
# y_output_classes = tf.placeholder()



def RNN_network_model(data):
    layer = {
        'weights' : tf.Variable(tf.random_normal([rnn_size, n_classes])),
        'biases' : tf.Variable(tf.random_normal([n_classes]))
    }

    output = 5
    return output