\select@language {ngerman}
\select@language {ngerman}
\contentsline {chapter}{\nonumberline Abbildungsverzeichnis}{iii}
\contentsline {chapter}{\nonumberline Tabellenverzeichnis}{iv}
\contentsline {chapter}{\nonumberline Abk\"{u}rzungsverzeichnis}{v}
\contentsline {chapter}{\numberline {1}Einleitung}{1}
\contentsline {section}{\numberline {1.1}Ziel und Aufbau der Arbeit}{1}
\contentsline {chapter}{\numberline {2}Neuronale Netze und deren Bestandteile}{3}
\contentsline {section}{\numberline {2.1}Aufbau eines Neurons}{3}
\contentsline {section}{\numberline {2.2}Feedforward Netz}{5}
\contentsline {chapter}{\numberline {3}Datenaufbereitung und Parameter der Neuronalen Netze}{10}
\contentsline {section}{\numberline {3.1}Datenaufbereitung}{10}
\contentsline {section}{\numberline {3.2}Parameter des neuronalen Netze}{11}
\contentsline {chapter}{\numberline {4}Evaluation des Modells}{13}
\contentsline {chapter}{\numberline {5}Zusammenfassung der Ergebnisse}{17}
\select@language {ngerman}
\contentsline {chapter}{\nonumberline Literaturverzeichnis}{vi}
\contentsline {part}{Anhang}{viii}
\contentsline {section}{\numberline {1}Main}{ix}
\contentsline {section}{\numberline {2}Data Provider}{x}
\contentsline {section}{\numberline {3}Random}{xx}
\contentsline {section}{\numberline {4}MLP Model}{xxii}
\contentsline {section}{\numberline {5}Utils}{xxxi}
\contentsline {section}{\numberline {6}Configuration}{xxxii}
